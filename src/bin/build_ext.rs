use std::{fs::File, io::Write, path::PathBuf, process::Command};

use serde::Serialize;

#[derive(Serialize)]
struct ExtensionContext {
    hash: String,
}

#[derive(Serialize)]
struct ContentScript {
    matches: Vec<String>,
    js: Vec<String>,
}

#[derive(Serialize)]
struct Manifest {
    manifest_version: u32,
    name: String,
    version: String,
    content_scripts: Vec<ContentScript>,
    web_accessible_resources: Vec<String>,
}
/// See more at https://meet-martin.medium.com/using-javascript-es6-import-export-modules-in-chrome-extensions-f63a3a0d2736
const INJECT_MODULE_LOAD: &str = r#"
'use strict';

const script = document.createElement('script');
script.setAttribute("type", "module");
script.setAttribute("src", chrome.extension.getURL('main.js'));
const head = document.head || document.getElementsByTagName("head")[0] || document.documentElement;
head.insertBefore(script, head.lastChild);

"#;

const MAIN_JS: &str = r#"
import init from '/ff-ext-{hash}.js';init()
"#;

fn main() {
    std::fs::remove_dir_all("dist").unwrap();
    Command::new("trunk")
        .args(["build"])
        .spawn()
        .unwrap()
        .wait()
        .unwrap();

    let generated_js = PathBuf::from("dist")
        .read_dir()
        .unwrap()
        .filter_map(|path| path.ok())
        .map(|de| de.file_name().to_str().unwrap().to_string())
        .find(|d| d.ends_with(".js"))
        .unwrap();

    let hash = generated_js
        .rsplit_once('.')
        .unwrap()
        .0
        .rsplit_once('-')
        .unwrap()
        .1;

    let mut tt = tinytemplate::TinyTemplate::new();
    tt.add_template("main_js", MAIN_JS).unwrap();

    File::create("dist/inject_module_load.js")
        .unwrap()
        .write_all(INJECT_MODULE_LOAD.as_bytes())
        .unwrap();
    {
        let mut f = File::create("dist/manifest.json").unwrap();
        f.write_all(
            serde_json::to_string_pretty(&Manifest {
                manifest_version: 2,
                name: "my_test_extension".into(),
                version: "1.0".into(),
                content_scripts: vec![ContentScript {
                    matches: vec!["<all_urls>".to_owned()],
                    js: vec!["inject_module_load.js".to_string()],
                }],
                web_accessible_resources: vec![
                    "main.js".to_string(),
                    format!("ff-ext-{hash}.js"),
                    format!("ff-ext-{hash}_bg.wasm"),
                ],
            })
            .unwrap()
            .as_bytes(),
        )
        .unwrap();
    }
    {
        File::create("dist/main.js")
            .unwrap()
            .write_all(
                tt.render(
                    "main_js",
                    &ExtensionContext {
                        hash: hash.to_string(),
                    },
                )
                .unwrap()
                .as_bytes(),
            )
            .unwrap();
    }

    Command::new("zip")
        .current_dir("dist")
        .args([
            "ext.zip",
            "inject_module_load.js",
            "manifest.json",
            &"ff-ext-{hash}.js".replace("{hash}", hash),
            &"ff-ext-{hash}_bg.wasm".replace("{hash}", hash),
            "main.js",
        ])
        .spawn()
        .unwrap()
        .wait()
        .unwrap();
}
