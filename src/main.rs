use wasm_bindgen::{prelude::Closure, JsCast};
use web_sys::HtmlInputElement;

fn main() {
    std::panic::set_hook(Box::new(console_error_panic_hook::hook));

    let document = web_sys::window().unwrap().document().unwrap();
    let h1 = document.query_selector("h1").unwrap().unwrap();
    h1.set_inner_html("Hello! I am breaking your stuff without gloo!");
    let input_element = document
        .create_element("input")
        .unwrap()
        .dyn_into::<HtmlInputElement>()
        .unwrap();
    input_element.set_attribute("type", "file").unwrap();
    let input_element_clone = input_element.clone();
    let callback = Closure::<dyn Fn()>::new(move || {
        if let Some(files) = input_element_clone.files() {
            let file = files.item(0).unwrap();
            panic!("TODO: load the file {}", file.name());
        }
    });
    input_element
        .add_event_listener_with_callback("change", callback.into_js_value().unchecked_ref())
        .unwrap();

    let body = document.query_selector("body").unwrap().unwrap();
    body.append_child(&input_element).unwrap();

    // None::<()>.unwrap_throw();
}
